package ru.hnau.remote_teaching_server.api

import org.springframework.http.HttpHeaders
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.remote_teaching_common.api.ApiResponse
import ru.hnau.remote_teaching_common.api.ApiUtils
import ru.hnau.remote_teaching_common.data.ClientType
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.ddos_validator.IpDdosValidator
import ru.hnau.remote_teaching_server.api.ddos_validator.UserDdosValidator
import ru.hnau.remote_teaching_server.db.entities.UserDB
import ru.hnau.remote_teaching_server.utils.Utils
import ru.hnau.remote_teaching_server.utils.log.HSessionLog
import ru.hnau.remote_teaching_server.utils.stringStackTrace


object RestServiceUtils {

    private const val MIN_SUPPORTED_CLIENT_VERSION = 1
    private const val LAST_VERSION = 1

    fun <T : Any> handleRequest(
            restServiceContext: RestServiceContext,
            headers: HttpHeaders,
            weight: DdosRequestWeight,
            authenticationExpected: Boolean,
            requiredPermissions: Iterable<UserPermission>,
            availableForNotConfiguredAdmin: Boolean = false,
            handler: RequestContext.() -> T
    ): ApiResponse<T> {
        val sessionLog = HSessionLog(Utils.genBase64UUID())
        return try {
            val response = handleRequestUnsafe(
                    restServiceContext,
                    sessionLog,
                    headers,
                    weight,
                    authenticationExpected,
                    requiredPermissions,
                    availableForNotConfiguredAdmin,
                    handler
            )
            sessionLog.d("Finished with response: $response")
            ApiResponse.success(response)
        } catch (ex: Exception) {
            val apiException = if (ex is ApiException) {
                sessionLog.w("Finished with error: $ex")
                ex
            } else {
                sessionLog.e(ex.stringStackTrace)
                ApiException.UNDEFINED
            }
            ApiResponse.error(apiException)
        }
    }

    private fun <T : Any> handleRequestUnsafe(
            restServiceContext: RestServiceContext,
            sessionLog: HSessionLog,
            headers: HttpHeaders,
            weight: DdosRequestWeight,
            authenticationExpected: Boolean,
            requiredPermissions: Iterable<UserPermission>,
            availableForNotConfiguredAdmin: Boolean = false,
            handler: RequestContext.() -> T
    ): T {

        val ip: String? = (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request.remoteAddr
        sessionLog.d("Request from: $ip")

        IpDdosValidator.validate(ip, weight)

        val userDB = authenticationExpected.ifTrue {
            val user = getUserByHeaders(headers, restServiceContext)
            UserDdosValidator.validate(user.login, weight)
            validateIsConfiguredIfAdmin(user, availableForNotConfiguredAdmin)
            return@ifTrue user
        }
        val user = userDB?.user

        validateClientTypeByHeaders(headers)
        validateClientVersionByHeaders(headers)

        user?.let { validateUserPermissions(it, requiredPermissions) }

        return handler.invoke(RequestContext(userDB, sessionLog))
    }

    @Throws(ApiException::class)
    private fun getUserByHeaders(
            headers: HttpHeaders,
            restServiceContext: RestServiceContext
    ) = headers[ApiUtils.HEADER_AUTH_TOKEN]
            ?.firstOrNull()
            ?.takeIfNotEmpty()
            ?.let(restServiceContext.userManager::getByAuthToken)
            ?: throw ApiException.AUTHENTICATION

    @Throws(ApiException::class)
    private fun validateClientTypeByHeaders(headers: HttpHeaders) {
        val clientType = headers[ApiUtils.HEADER_CLIENT_TYPE]?.firstOrNull()
        if (clientType == null || clientType !in ClientType.supportedIdentifiers) {
            throw ApiException.raw("Неподдерживаемый тип клиента '$clientType'")
        }
    }

    @Throws(ApiException::class)
    private fun validateClientVersionByHeaders(headers: HttpHeaders) {
        val clientVersion = headers[ApiUtils.HEADER_CLIENT_VERSION]?.firstOrNull()?.toIntOrNull()
        if (clientVersion == null || clientVersion < MIN_SUPPORTED_CLIENT_VERSION) {
            throw ApiException.unsupportedVersion(
                    oldVersion = clientVersion ?: 0,
                    comment = "",
                    newVersion = LAST_VERSION
            )
        }
    }

    @Throws(ApiException::class)
    private fun validateUserPermissions(
            user: User,
            requiredPermissions: Iterable<UserPermission>
    ) = requiredPermissions.forEach { requiredPermission ->
        if (requiredPermission !in user.role.permissions) {
            throw ApiException.raw("${requiredPermission.title} недоступно для роли ${user.role.name}")
        }
    }

    @Throws(ApiException::class)
    private fun validateIsConfiguredIfAdmin(
            user: UserDB,
            availableForNotConfiguredAdmin: Boolean
    ) {
        if (user.role != UserRole.ADMIN || availableForNotConfiguredAdmin) {
            return
        }
        if (user.passwordHash == null || user.passwordHash == UserDB.getPasswordHash(User.DEFAULT_ADMIN_PASSWORD)) {
            throw ApiException.ADMIN_PASSWORD_NOT_CONFIGURED
        }
    }

}