package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.user.StudentManager


@RestController
class StudentsListRestService : RestService() {

    @Autowired
    private lateinit var studentManager: StudentManager

    @DeleteMapping("/students/{student-login}")
    fun delete(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("student-login") studentLogin: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_STUDENTS)) {

        Validators.validateUserLoginOrThrow(studentLogin)

        studentManager.deleteStudent(studentLogin)
    }


    @PostMapping("/students/{student-login}/restore-password-code")
    fun restorePasswordCode(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("student-login") studentLogin: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_STUDENTS)) {

        Validators.validateUserLoginOrThrow(studentLogin)

        studentManager.generateRestoreStudentPasswordActionCode(
                login = studentLogin,
                sessionLogger = logger
        )
    }

    @PostMapping("/students/me/restore-password")
    fun restorePassword(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("new-password") newPassword: String,
            @RequestParam("action-code") actionCode: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, null) {

        Validators.validateUserPasswordOrThrow(newPassword)

        studentManager.restoreStudentPassword(
                code = actionCode,
                newPassword = newPassword,
                sessionLogger = logger
        )
    }

    @PutMapping("/students")
    fun create(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("students-group-name") studentsGroupName: String
    ) = handleRequest(headers, DdosRequestWeight.MEDIUM, listOf(UserPermission.MANAGE_STUDENTS, UserPermission.MANAGE_GROUPS)) {

        Validators.validateStudentsGroupNameOrThrow(studentsGroupName)

        studentManager.generateCreateStudentActionCode(
                studentsGroupName = studentsGroupName,
                sessionLogger = logger
        )
    }

    @PutMapping("/students/{login}")
    fun register(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("login") login: String,
            @RequestParam("password") password: String,
            @RequestParam("action-code") actionCode: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, null) {

        Validators.validateUserLoginOrThrow(login)
        Validators.validateUserPasswordOrThrow(password)

        studentManager.createStudent(
                login = login,
                actionCode = actionCode,
                password = password,
                sessionLogger = logger
        )
    }

}