package ru.hnau.remote_teaching_server.api.services

import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService


@RestController
class ConfigUserRestService : RestService() {

    @PostMapping("/users/me/change-password")
    fun changePassword(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("new-password") newPassword: String
    ) = handleRequest(
            headers, DdosRequestWeight.HARD, emptyList(),
            availableForNotConfiguredAdmin = true
    ) {

        Validators.validateUserPasswordOrThrow(newPassword)

        userManager.updatePassword(user.login, newPassword)
    }

    @PostMapping("/users/me/change-fio")
    fun changeFIO(
            @RequestHeader headers: HttpHeaders,
            @RequestParam("new-name") newName: String,
            @RequestParam("new-surname") newSurname: String,
            @RequestParam("new-patronymic") newPatronymic: String
    ) = handleRequest(
            headers, DdosRequestWeight.MEDIUM, emptyList()
    ) {

        Validators.validateUserNameOrThrow(newName)
        Validators.validateUserSurnameOrThrow(newSurname)
        Validators.validateUserPatronymicOrThrow(newPatronymic)

        userManager.updateFio(user.login, newName, newSurname, newPatronymic)
    }

}