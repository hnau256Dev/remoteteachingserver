package ru.hnau.remote_teaching_server.api.services.base

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_server.api.RequestContext
import ru.hnau.remote_teaching_server.api.RestServiceContext
import ru.hnau.remote_teaching_server.api.RestServiceUtils
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.managers.internal.user.UserManager


abstract class RestService {

    @Autowired
    protected lateinit var userManager: UserManager

    protected val restServiceContext: RestServiceContext by lazy {
        RestServiceContext(userManager)
    }

    fun <T : Any> handleRequest(
            headers: HttpHeaders,
            weight: DdosRequestWeight = DdosRequestWeight.MEDIUM,
            permissions: Iterable<UserPermission>? = null,
            availableForNotConfiguredAdmin: Boolean = false,
            handler: RequestContext.() -> T
    ) = RestServiceUtils.handleRequest(
            restServiceContext = restServiceContext,
            headers = headers,
            weight = weight,
            authenticationExpected = permissions != null,
            requiredPermissions = permissions ?: emptyList(),
            availableForNotConfiguredAdmin = availableForNotConfiguredAdmin,
            handler = handler
    )


}