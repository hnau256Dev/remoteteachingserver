package ru.hnau.remote_teaching_server.api.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.web.bind.annotation.*
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_common.data.UserPermission
import ru.hnau.remote_teaching_server.api.ddos_validator.DdosRequestWeight
import ru.hnau.remote_teaching_server.api.services.base.RestService
import ru.hnau.remote_teaching_server.managers.internal.StudentsGroupManager
import ru.hnau.remote_teaching_server.managers.internal.user.StudentManager


@RestController
class StudentsGroupsListManager : RestService() {

    @Autowired
    private lateinit var studentsGroupManager: StudentsGroupManager

    @Autowired
    private lateinit var studentManager: StudentManager


    @GetMapping("/students-groups")
    fun groupsList(
            @RequestHeader headers: HttpHeaders
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, listOf(UserPermission.MANAGE_GROUPS)) {
        studentsGroupManager.getAll()
    }

    @GetMapping("/students-groups/{students-group-name}/students")
    fun studentsList(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("students-group-name") studentsGroupName: String
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, listOf(UserPermission.MANAGE_GROUPS)) {

        Validators.validateStudentsGroupNameOrThrow(studentsGroupName)

        studentManager.getAllStudentsOfGroup(studentsGroupName)
    }

    @PutMapping("/students-groups/{students-group-name}")
    fun createGroup(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("students-group-name") studentsGroupName: String
    ) = handleRequest(headers, DdosRequestWeight.HARD, listOf(UserPermission.MANAGE_GROUPS)) {

        Validators.validateStudentsGroupNameOrThrow(studentsGroupName)

        studentsGroupManager.create(studentsGroupName)
    }

    @PostMapping("/students-groups/{students-group-name}/archive")
    fun archiveGroup(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("students-group-name") studentsGroupName: String
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, listOf(UserPermission.MANAGE_GROUPS)) {

        Validators.validateStudentsGroupNameOrThrow(studentsGroupName)

        studentsGroupManager.archive(studentsGroupName)
    }

    @PostMapping("/students-groups/{students-group-name}/unarchive")
    fun unarchiveGroup(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("students-group-name") studentsGroupName: String
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, listOf(UserPermission.MANAGE_GROUPS)) {

        Validators.validateStudentsGroupNameOrThrow(studentsGroupName)

        studentsGroupManager.unarchive(studentsGroupName)

    }

    @DeleteMapping("/students-groups/{students-group-name}")
    fun deleteGroup(
            @RequestHeader headers: HttpHeaders,
            @PathVariable("students-group-name") studentsGroupName: String
    ) = handleRequest(headers, DdosRequestWeight.LIGHT, listOf(UserPermission.MANAGE_GROUPS, UserPermission.MANAGE_STUDENTS)) {

        Validators.validateStudentsGroupNameOrThrow(studentsGroupName)

        studentsGroupManager.delete(studentsGroupName)
    }


}