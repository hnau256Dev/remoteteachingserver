package ru.hnau.remote_teaching_server.api.ddos_validator

import ru.hnau.remote_teaching_common.exception.ApiException


object UserDdosValidator : DdosValidator<String>() {

    override fun throwBlockedException(seconds: Long?) =
            throw ApiException.ddosBlockedUser(seconds)

}