package ru.hnau.remote_teaching_server.db.entities.utils

import org.springframework.data.annotation.Id
import ru.hnau.remote_teaching_server.utils.Utils


abstract class BindingCollection(
        @Id val key: String?
) {

    companion object {

        const val KEY_KEY = "key"

        fun createKey(vararg keyParts: String?) =
                Utils.getStringsKey(*keyParts)

    }

}