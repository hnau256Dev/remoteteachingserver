package ru.hnau.remote_teaching_server.db.entities

import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.remote_teaching_common.data.section.OpenedSection
import ru.hnau.remote_teaching_server.db.entities.utils.BindingCollection
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "opened_section")
class OpenedSectionDB(
        val sectionUUID: String? = null,
        val studentsGroupName: String? = null
) : BindingCollection(
        key = createKey(sectionUUID, studentsGroupName)
) {

    companion object : EntityFabric<OpenedSectionDB> {

        const val SECTION_UUID_KEY = "sectionUUID"
        const val STUDENTS_GROUP_NAME_KEY = "studentsGroupName"

        override val entityIdKey = KEY_KEY
        override val entityClass = OpenedSectionDB::class.java
        override val entityName = "Открытый для группы раздел"

        fun createKey(sectionUUID: String?, studentsGroupName: String?) =
                BindingCollection.createKey(sectionUUID, studentsGroupName)

    }

    val openedSection: OpenedSection
        get() = OpenedSection(
                sectionUUID = sectionUUID!!,
                studentsGroupName = studentsGroupName!!
        )

}