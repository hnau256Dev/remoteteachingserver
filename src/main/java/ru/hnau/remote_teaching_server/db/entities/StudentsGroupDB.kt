package ru.hnau.remote_teaching_server.db.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.remote_teaching_common.data.StudentsGroup
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "students_group")
data class StudentsGroupDB(
        @Id
        val name: String? = null,
        val archived: Boolean? = null
) {

    companion object : EntityFabric<StudentsGroupDB> {

        const val NAME_KEY = "name"
        const val ARCHIVED_KEY = "archived"

        override val entityIdKey = NAME_KEY
        override val entityClass = StudentsGroupDB::class.java
        override val entityName = "Студенческая группа"

    }

    val studentsGroup: StudentsGroup
        get() = StudentsGroup(
                name!!,
                archived ?: false
        )

}