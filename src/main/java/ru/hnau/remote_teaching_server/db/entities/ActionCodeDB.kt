package ru.hnau.remote_teaching_server.db.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric
import ru.hnau.remote_teaching_server.utils.Utils


@Document(collection = "action_code")
data class ActionCodeDB(
        @Id
        val key: String? = null,
        val type: ActionCodeType? = null,
        val created: Long? = null,
        val additionalData: String? = null
) {

    constructor(
            type: ActionCodeType,
            code: String,
            created: Long,
            additionalData: String = ""
    ) : this(
            key = createKey(type, code),
            type = type,
            created = created,
            additionalData = additionalData
    )

    companion object : EntityFabric<ActionCodeDB> {

        const val KEY_KEY = "key"
        const val TYPE_KEY = "type"
        const val CREATED_KEY = "created"
        const val ADDITIONAL_DATA_KEY = "additionalData"

        override val entityIdKey = KEY_KEY
        override val entityClass = ActionCodeDB::class.java
        override val entityName = "Код активации"

        fun createKey(type: ActionCodeType, code: String) =
                Utils.getStringsKey(type.name, code)

    }

}