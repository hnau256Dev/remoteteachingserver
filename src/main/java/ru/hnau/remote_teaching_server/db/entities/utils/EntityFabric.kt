package ru.hnau.remote_teaching_server.db.entities.utils


interface EntityFabric<T : Any> {

    val entityIdKey: String

    val entityClass: Class<T>

    val entityName: String

}