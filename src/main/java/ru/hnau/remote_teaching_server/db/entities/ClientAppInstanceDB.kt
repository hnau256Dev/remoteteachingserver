package ru.hnau.remote_teaching_server.db.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "client_app_instance")
data class ClientAppInstanceDB(
        @Id
        val uuid: String? = null,
        val userLogin: String? = null,
        val pushToken: String? = null
) {

    companion object : EntityFabric<ClientAppInstanceDB> {

        const val UUID_KEY = "uuid"
        const val USER_LOGIN_KEY = "userLogin"
        const val PUSH_TOKEN_KEY = "pushToken"

        override val entityIdKey = UUID_KEY
        override val entityClass = ClientAppInstanceDB::class.java
        override val entityName = "Экземпляр клиентского приложения"

    }

}