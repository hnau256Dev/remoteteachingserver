package ru.hnau.remote_teaching_server.db.entities

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "section")
data class SectionDB(
        @Id
        val uuid: String? = null,
        val parentUUID: String? = null,
        val titleMD: String? = null,
        val contentMD: String? = null
) {

    companion object : EntityFabric<SectionDB> {

        const val UUID_KEY = "uuid"
        const val PARENT_UUID_KEY = "parentUUID"
        const val TITLE_MD_KEY = "titleMD"
        const val CONTENT_MD_KEY = "contentMD"

        override val entityIdKey = UUID_KEY
        override val entityClass = SectionDB::class.java
        override val entityName = "Секция"

    }

}