package ru.hnau.remote_teaching_server.db.entities


import org.apache.commons.codec.digest.DigestUtils
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_server.db.entities.utils.EntityFabric


@Document(collection = "user")
data class UserDB(
        @Id
        val login: String? = null,
        val passwordHash: String? = null,
        val authToken: String? = null,
        val name: String? = null,
        val surname: String? = null,
        val patronymic: String? = null,
        val studentsGroupName: String? = null,
        val role: UserRole? = null
) {

    companion object : EntityFabric<UserDB> {

        const val LOGIN_KEY = "login"
        const val PASSWORD_HASH_KEY = "passwordHash"
        const val AUTH_TOKEN_KEY = "authToken"
        const val NAME_KEY = "name"
        const val SURNAME_KEY = "surname"
        const val PATRONYMIC_KEY = "patronymic"
        const val STUDENTS_GROUP_NAME_KEY = "studentsGroupName"
        const val ROLE_KEY = "role"

        override val entityIdKey = LOGIN_KEY
        override val entityClass = UserDB::class.java
        override val entityName = "Пользователь"

        private const val HASH_SALT = "766303ab6b3846d681a3ffe015944a77"

        fun getPasswordHash(password: String) =
                DigestUtils.sha256Hex(password + HASH_SALT)!!

    }

    val user: User
        get() = User(
                login!!,
                name!!,
                surname!!,
                patronymic!!,
                studentsGroupName!!,
                role!!
        )

}