package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.query.lt
import ru.hnau.remote_teaching_server.db.entities.ActionCodeDB


@Component
class ActionCodeDbConnector : DbConnector<ActionCodeDB, String>(
        entityFabric = ActionCodeDB
) {

    fun put(actionCode: ActionCodeDB) {
        save(actionCode)
    }

    fun findOrNull(type: ActionCodeType, code: String) =
            findFirstOrNull(checkId(type, code))

    fun exists(type: ActionCodeType, code: String) =
            exists(checkId(type, code))

    fun delete(type: ActionCodeType, code: String) =
            delete(checkId(type, code)) > 0

    fun deleteAllWithTypeAndCreatedBeforeTime(
            type: ActionCodeType,
            beforeTime: Long
    ) = delete(
            checkType(type) +
                    (ActionCodeDB.CREATED_KEY lt beforeTime)
    )

    fun deleteAllWithTypeAndAdditionalData(
            type: ActionCodeType,
            additionalData: String
    ) {
        delete(
                checkType(type) +
                        (ActionCodeDB.ADDITIONAL_DATA_KEY eq additionalData)
        )
    }

    private fun checkId(type: ActionCodeType, code: String) =
            checkId(ActionCodeDB.createKey(type, code))

    private fun checkType(type: ActionCodeType) =
            ActionCodeDB.TYPE_KEY eq type

}