package ru.hnau.remote_teaching_server.db.connectors.utils.query

import org.springframework.data.mongodb.core.query.Query


interface DbQueryBuilder {

    fun buildQuery(): Query

}
