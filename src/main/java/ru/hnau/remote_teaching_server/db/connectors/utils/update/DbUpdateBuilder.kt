package ru.hnau.remote_teaching_server.db.connectors.utils.update

import org.springframework.data.mongodb.core.query.Update


interface DbUpdateBuilder {

    fun buildUpdate(): Update

}