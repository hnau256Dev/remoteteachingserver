package ru.hnau.remote_teaching_server.db.connectors

import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.utils.DbConnector
import ru.hnau.remote_teaching_server.db.connectors.utils.query.eq
import ru.hnau.remote_teaching_server.db.connectors.utils.update.set
import ru.hnau.remote_teaching_server.db.entities.StudentsGroupDB


@Component
class StudentsGroupDbConnector : DbConnector<StudentsGroupDB, String>(
        entityFabric = StudentsGroupDB
) {

    fun getAll() =
            findAll()

    fun create(name: String) =
            save(StudentsGroupDB(name, false))

    fun throwIfAlreadyExists(name: String) =
            throwIfExists(checkId(name))

    fun throwIfNotExists(name: String) =
            throwIfNotExists(checkId(name))

    fun throwIfNotArchived(name: String) = throwIfNotExists(
            checkId(name) +
                    (StudentsGroupDB.ARCHIVED_KEY eq true)
    )

    fun throwIfArchived(name: String) = throwIfNotExists(
            checkId(name) +
                    (StudentsGroupDB.ARCHIVED_KEY eq false)
    )

    fun getOrThrow(name: String) =
            findFirstOrThrow(checkId(name))

    fun getNotArchivedOrThrow(name: String) = findFirstOrThrow(
            checkId(name) +
                    (StudentsGroupDB.ARCHIVED_KEY eq false)
    )

    fun setIsArchived(
            name: String,
            archived: Boolean
    ) = updateFirst(
            checkId(name),
            StudentsGroupDB.ARCHIVED_KEY set archived
    )

    fun delete(name: String) =
            delete(checkId(name)) > 0


}