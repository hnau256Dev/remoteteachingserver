package ru.hnau.remote_teaching_server.db.connectors.utils.update

import org.springframework.data.mongodb.core.query.Update


class DbUpdates(
        private val updates: Iterable<DbUpdate>
) : DbUpdateBuilder {

    constructor(
            update: DbUpdate
    ) : this(
            listOf(update)
    )

    override fun buildUpdate() = Update().apply {
        updates.forEach { (key, value) -> set(key, value) }
    }

    operator fun plus(update: DbUpdate) =
            DbUpdates(updates + update)

    operator fun plus(other: DbUpdates) =
            DbUpdates(updates + other.updates)

}

infix fun String.set(value: Any) =
        DbUpdates(DbUpdate(this, value))