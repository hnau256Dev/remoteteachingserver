package ru.hnau.remote_teaching_server.managers.internal.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_common.exception.ApiException
import ru.hnau.remote_teaching_server.db.connectors.UserDbConnector
import ru.hnau.remote_teaching_server.db.entities.UserDB
import ru.hnau.remote_teaching_server.managers.internal.ActionCodeManager
import ru.hnau.remote_teaching_server.managers.internal.ClientAppInstanceManager
import ru.hnau.remote_teaching_server.managers.internal.StudentsGroupManager
import ru.hnau.remote_teaching_server.utils.Utils
import ru.hnau.remote_teaching_server.utils.log.HSessionLog


@Component
class TeacherManager {

    @Autowired
    private lateinit var dbConnector: UserDbConnector

    @Autowired
    private lateinit var userManager: UserManager

    @Autowired
    private lateinit var actionCodeManager: ActionCodeManager

    @Autowired
    private lateinit var studentsGroupManager: StudentsGroupManager

    fun getAllTeachers() =
            dbConnector.findAllTeachers().toUsersList()

    fun generateCreateTeacherActionCode(
            sessionLogger: HSessionLog
    ): String {
        return actionCodeManager.create(
                type = ActionCodeType.CREATE_TEACHER,
                sessionLogger = sessionLogger
        )
    }

    fun generateRestoreTeacherPasswordActionCode(
            login: String,
            sessionLogger: HSessionLog
    ): String {

        dbConnector.throwIfNotTeacher(login)

        return actionCodeManager.create(
                type = ActionCodeType.RESTORE_TEACHER_PASSWORD,
                additionalData = login,
                sessionLogger = sessionLogger
        )
    }

    fun restoreTeacherPassword(
            code: String,
            newPassword: String,
            sessionLogger: HSessionLog
    ): String {

        val login = actionCodeManager.use(
                code = code,
                type = ActionCodeType.RESTORE_TEACHER_PASSWORD,
                sessionLogger = sessionLogger
        )

        userManager.updatePassword(login, newPassword)

        return login
    }

    fun createTeacher(
            login: String,
            password: String,
            actionCode: String,
            sessionLogger: HSessionLog
    ) {
        dbConnector.throwIfAlreadyExists(login)

        actionCodeManager.use(
                code = actionCode,
                type = ActionCodeType.CREATE_TEACHER,
                sessionLogger = sessionLogger
        )

        userManager.createNew(
                login = login,
                password = password,
                role = UserRole.TEACHER
        )
    }


    fun deleteTeacher(login: String) {
        dbConnector.throwIfNotTeacher(login)
        dbConnector.delete(login)
    }

}