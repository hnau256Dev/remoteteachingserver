package ru.hnau.remote_teaching_server.managers.internal

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import ru.hnau.remote_teaching_server.db.connectors.StudentsGroupDbConnector
import ru.hnau.remote_teaching_server.managers.internal.user.StudentManager
import ru.hnau.remote_teaching_server.managers.internal.user.UserManager


@Component
class StudentsGroupManager {

    @Autowired
    private lateinit var dbConnector: StudentsGroupDbConnector

    @Autowired
    private lateinit var userManager: UserManager

    @Autowired
    private lateinit var studentManager: StudentManager

    fun getAll() =
            dbConnector.getAll().map { it.studentsGroup }

    fun throwIfArchived(name: String) =
            dbConnector.throwIfArchived(name)

    fun throwIfNotExists(name: String) =
            dbConnector.throwIfNotExists(name)

    fun create(name: String) {
        dbConnector.throwIfAlreadyExists(name)
        dbConnector.create(name)
    }

    fun delete(name: String) {
        dbConnector.throwIfNotArchived(name)
        studentManager.deleteAllStudentsOfGroup(name)
        //TODO delete all opened sections of group
        dbConnector.delete(name)
    }

    fun archive(name: String) {
        throwIfArchived(name)
        dbConnector.setIsArchived(name, true)
    }

    fun unarchive(name: String) {
        dbConnector.throwIfNotArchived(name)
        dbConnector.setIsArchived(name, false)
    }

}