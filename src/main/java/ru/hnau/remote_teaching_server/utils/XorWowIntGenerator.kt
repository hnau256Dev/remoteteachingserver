package ru.hnau.remote_teaching_server.utils


class XorWowIntGenerator(seed: Int) {

    private var x = seed
    private var y = x shr 31
    private var z = 0
    private var w = 0
    private var v = x.inv()
    private var a = (x shl 10) xor (y ushr 4)

    init {
        repeat(64) { next() }
    }

    fun next(): Int {
        var t = x
        x = y
        y = z
        z = w
        w = v
        t = t xor (t ushr 2)
        t = (t xor (t shl 1)) xor v xor (v shl 4)
        v = t
        a += 362437
        return t + a
    }

}