package ru.hnau.remote_teaching_server.utils.log


abstract class HLogWrapper(
        private val innerLogger: HLog
) : HLog {

    override fun i(msg: String, th: Throwable?) = innerLogger.i(formatMessage(msg), th)

    override fun t(msg: String, th: Throwable?) = innerLogger.t(formatMessage(msg), th)

    override fun d(msg: String, th: Throwable?) = innerLogger.d(formatMessage(msg), th)

    override fun w(msg: String, th: Throwable?) = innerLogger.w(formatMessage(msg), th)

    override fun e(msg: String, th: Throwable?) = innerLogger.e(formatMessage(msg), th)

    protected abstract fun formatMessage(msg: String): String

}