package ru.hnau.remote_teaching_server.utils.log

import org.apache.logging.log4j.LogManager


object HBaseLog : HLog {

    private val logger = LogManager.getLogger()

    override fun i(msg: String, th: Throwable?) = logger.info(msg, th)

    override fun t(msg: String, th: Throwable?) = logger.trace(msg, th)

    override fun d(msg: String, th: Throwable?) = logger.debug(msg, th)

    override fun w(msg: String, th: Throwable?) = logger.warn(msg, th)

    override fun e(msg: String, th: Throwable?) = logger.error(msg, th)

    fun getSessionLog(sessionId: String) = HSessionLog(sessionId)

}